#!/bin/sh
export DEPLOYMENT=${DEPLOYMENT:-api}

echo "[LOG] Deploying ${DEPLOYMENT}"
echo "[LOG] Using database: ${DATABASE_URL}"
echo "[LOG] Using redis: ${REDIS_URL}"

if [ "$DEPLOYMENT" == "api" ]
then
    echo "[LOG] Preparing database"
    python manage.py prepare_kubernetes_db
    echo "[LOG] Running migrations"
    python manage.py db upgrade
    export PORT=${PORT:-8080}
    echo "[LOG] Starting uwsgi on port ${PORT}"
    uwsgi --socket 0.0.0.0:${PORT} --protocol=http --chdir=/data/app --enable-threads -w app:app
fi
if [ "$DEPLOYMENT" == "celery" ]
then
    echo "[LOG] Starting celery worker"
    export INTEGRATE_SOCKETIO=false
    celery worker -A app.celery --loglevel=warning
fi
